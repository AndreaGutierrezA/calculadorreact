import "./App.css";
// importacon de una imagen y le asigamos esta propiedad freeCodeCampLogo
import freeCodeCampLogo from "./imagenes/freecodecamp-logo.png";
import Pantalla from "./componentes/Pantalla";
import Boton from "./componentes/Boton";
import BotonClear from "./componentes/BotonClear";
/* useState es el Hoocks para el estado del componente*/
import { useState } from "react";
/* evaluate hace parte de un paquete llamado mathjs que es una libreria o biblioteca de funcione matematicas para javascript y node js,
se debe realizar la instalacion nom install mathjs*/
import { evaluate } from "mathjs";

function App() {
  /* Trabajar el estado, con lo que el usuario a ingresado */
  /*  se crea un estado para la aplicacion OSE EL ESTADO DEL COMPONENTE (App) llamado INPUT */
  /* Funciono para actualizar el estado, ose el INPUT*/
  /*  Inicialmente el ESTADO (INPUT) es una cadena vacia '' */

  /* se debe asignar el INPUT como el valor de la pantalla */
  const [inputEstado, setInput] = useState("");
 
  /* DEFINIR  FUNCION Y USARLA PARA PASARLA COMO EL VALOR DEL PROPS */
  /* FUNCION agregarInput que sera una funcion felcha que va a tomar un valor (val) y va ejecutar una secuencia de lineas que va a estar dentro de las llaves */
  const agregarInput = (val) => {
    /* se usa la funcion setInput que es la que va a actualizar el estado,
       tiene como argumento el valor del estado + el nuevo estado*/
    setInput(inputEstado + val);
  };

  /* FUNCION FLECHA calcularResultado */
  const calcularResultado = () => {
    /* si inputEstado no es vacio realiza la operacion  setInput(evaluate(inputEstado));*/
    if (inputEstado) {
      /* EVALUAR lo que esta en la pantalla osea a el inputEstado y lo calcula*/
      setInput(evaluate(inputEstado));
    } else {
      /* inputEstado si es '' entonces se muestra una alerta */
      alert("Por favor ingrese valores para realizar los cálculos.");
    }
  };

  return (
    <div className="App">
      <div className="freecodecamp-logo-contenedor">
        <img
          /* forma de agregar una imagen con la propiedad freeCodeCampLogo*/
          src={freeCodeCampLogo}
          className="freecodecamp-logo"
          alt="Logo de freeCodeCamp"
        />
      </div>
      <div className="contenedor-calculadora">
        {/* Crear la pantalla, la cual recibe el ESTADO......input es el PROPS y {inputEstado} es el ESTADO */}
        <Pantalla inputProp={inputEstado} />
        {/* se define las filas de la calculadora, las fila contiene los botones */}
        <div className="fila">
          {/* Crear los Botones en cada FILA */}
          {/* al usar el componente de BOTON se usar la etiqueta de cierre </> para cololcar el numero #
              que tendra el Boton, ese numero se considera como el Children*/}
          {/* se el pasa un PROPP al componente BOTON,en este caso se le asigna un PROPS llamado manejarClic
              que LLAMARA la FUNCION -> agregarInput
              */}
          <Boton manejarClic={agregarInput}>1</Boton>
          <Boton manejarClic={agregarInput}>2</Boton>
          <Boton manejarClic={agregarInput}>3</Boton>
          <Boton manejarClic={agregarInput}>+</Boton>
          {/* OPERADOR */}
        </div>
        <div className="fila">
          <Boton manejarClic={agregarInput}>4</Boton>
          <Boton manejarClic={agregarInput}>5</Boton>
          <Boton manejarClic={agregarInput}>6</Boton>
          <Boton manejarClic={agregarInput}>-</Boton>
        </div>
        <div className="fila">
          <Boton manejarClic={agregarInput}>7</Boton>
          <Boton manejarClic={agregarInput}>8</Boton>
          <Boton manejarClic={agregarInput}>9</Boton>
          <Boton manejarClic={agregarInput}>*</Boton>
        </div>
        <div className="fila">
          {/* el props manejarClic sera igual a una FUNCION */}
          <Boton manejarClic={calcularResultado}>=</Boton>
          <Boton manejarClic={agregarInput}>0</Boton>
          <Boton manejarClic={agregarInput}>.</Boton>
          <Boton manejarClic={agregarInput}>/</Boton>
        </div>
        <div className="fila">
          {/* Creacion del componente BOTONCLEAR */}
          {/* manejarClear es un PROPS tiene como valor una FUNCION anonima (() => setInput(""))que va a REINCIAR el INPUT con una cadena de CARACTERES VACIAS*/}
          <BotonClear manejarClear={() => setInput("")}>Clear</BotonClear>
        </div>
      </div>
    </div>
  );
}

export default App;
