import React from 'react';
import '../hoja-de-estilo/BotonClear.css';

/* otra manera de DEFINIR un COMPONENTES ES usando una
   CONSTANTE y la FUNCION FLECHA pero solo para componentes sencillo */
   /* recibe un props */
const BotonClear = (props) => (
  /* onClick es el EventListener para realizar su funcionalidad, RECIBE la FUNCION enviada como un PROPS props.manejarClear*/
  <div className='boton-clear' onClick={props.manejarClear}>
    {props.children}
    {/* Clear como es fijo puede ir en ves de props.children */}
  </div>
);

export default BotonClear;