import React from 'react';
import '../hoja-de-estilo/Pantalla.css'

/* otra manera de DEFINIR un COMPONENTES ES usando una
   CONSTANTE y la FUNCION FLECHA pero solo para componentes sencillo */
   /* recibe un props llamada inputProp el cual es el valor ingresado por el usuario */
const Pantalla = ({ inputProp }) => (
  /* clase input */
  <div className='input'>
    {inputProp}
  </div>
); 

export default Pantalla;