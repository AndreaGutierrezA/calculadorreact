import React from 'react';
import '../hoja-de-estilo/Boton.css';

/* creacion del componente funcional Boton, reciben cierto props */
function Boton(props) {

  /* se define una constante que tendra como valor lo que retorne la función flecha */
  /* isNaN es una funcion que sirve para  confirma si una cadena de CARACTERES NO ES UN NUMERO Y UN PUNTO Y 
     SI TAMPOCO ES EL SIGNO =, si no es ninguna de las anteriores entonces se pueden considerar como un OPERADOR*/
  const esOperador = valor => {
    return isNaN(valor) && (valor !== '.') && (valor !== '=');
    /* Retorna FALSO o Verdadero que es lo q se usa para saber q clase toma en  className del DIV*/
  };

  return (
    <div
    /* todo lo que este incluido entre la etiqueta de apertura y cierre
       se puede considerar como un  children -> props.children es  */
    
       /*className contiene `` que son para definir platilla literales*/
       /* clase  boton-contenedor*/
       /* esOperador es una funcion que recibe el valor que tiene CHILDREN, si es numero o un operador
          dependiendo de ese VALOR se le asigna la clase, en este casi si es VERDADERO se le asigna la clase operador
          que es otra clase que se le asigna a ese boton */

          /* .trimEnd() es un metodo q sirve para ELIMINAR el espacio que hay al final de una cadena
              de caracteres r $*/

              /* Agregar el OYENTE DE EVENTOS/ EVENTlISTERNER OSEA EL onClick que llamara una funcion anonima que no tiene ningun argumento ()
              como y que retorna => el resultado de llamar a la funcion props.manejarClic(props.children) */
      className={`boton-contenedor ${esOperador(props.children) ? 'operador' : ''}`.trimEnd()}
      onClick={() => props.manejarClic(props.children)}>
      {props.children}
    </div>
  );
}

export default Boton;